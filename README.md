[![Gem Version](https://badge.fury.io/rb/bundler.svg)](https://badge.fury.io/rb/bundler)
# Ruby installation in macbook #

Mac comes with ruby by default. If you just need ruby for basic scripting purposes, then do not bother to get any further.  Have Fun :)

Good Bye!!


## Our goal ##

Generally when we install a language interpreter and start using them, naturally the next we care more shall be programming and syntax and less on the Interpreter and their infrastructure.

My work involves mobile apps automation using calabash,which is a ruby gem by itself. Earlier times, when we develop automation in one's system and access the same repository from  other engineer's macbook or linux host, we spend significant time in resolving Ruby version and gem dependency issues. This is surely not a time sustaining and efficient ones. Then I came across good materials in internet, that helped me to manage ruby and its gems in a repeatable manner. I am going to share the same.

Our goal is to understand and have a repeatable ruby and gems environment.

## Ruby Installation options ##

One can install ruby in these ways :

![ruby-installation-options.png](https://bitbucket.org/repo/8zz59nx/images/2798683349-ruby-installation-options.png)

## Our choice ##
Ruby Version Manager, in short "rvm".


# Ruby Installation using RVM #

Let us do some preperation work by installing few  utilities that will enable communication between your localhost and rvm package server.

```
> brew install gpg
> \curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
```

Let us install rvm like this

```
> \curl -sSL https://get.rvm.io | bash -s stable --ruby
> source /Users/savita/.rvm/scripts/rvm
```

Let us upgrade rvm to its latest version
```
> rvm get stable --auto-dotfiles
```

one may verify successful installation like this:
```
> rvm --version
output: rvm 1.29.1 (latest) by Michal Papis, Piotr Kuczynski, Wayne E. Seguin [https://rvm.io/]
```


Now one may choose to install any version of ruby. To know list of available versions rubies from your version of rvm, issue this command and watch out for the results.
```
> rvm list known
output:  # MRI Rubies
[ruby-]1.8.6[-p420]
[ruby-]1.8.7[-head] # security released on head
[ruby-]1.9.1[-p431]
[ruby-]1.9.2[-p330]
[ruby-]1.9.3[-p551]
[ruby-]2.0.0[-p648]
[ruby-]2.1[.10]
[ruby-]2.2[.6]
[ruby-]2.3[.3]
[ruby-]2.4[.0]
ruby-head
...
...
...
```

From the above results, I choose to install most latest version (2.4.0) by choice.

To install ruby, issue this command
```
> rvm install ruby-2.4.0
```

One may verfy successful installation of ruby like this.
```
> which ruby
output: /Users/savita/.rvm/rubies/ruby-2.4.0/bin/ruby
```

# Ruby Gems #

Gems to ruby are like jar library to java.
One can install ruby gems individually like how we import jar files into a java project.

## Primitive Gem Installation ##

Locate ruby gems in https://rubygems.org/

Example installation
```
> gem install nokogiri
```

such installed gems are stored in ~/.rvm/rubies/default/lib/ruby/gems/<ruby_version>/bin/

If this gem has dependency on other gems, then user has to install them manually in the expected order.


However maintaining gem dependency manually is not a pleasent experience. So let us get used to a better way.

## Gem Installation using bundler ##

There is a gem namely "bundler" which in turn manages further gem installation in a better way.
Let us install bundler gem first

```
> gem install bundler
Output: Fetching: bundler-1.14.6.gem (100%)
Successfully installed bundler-1.14.6
Parsing documentation for bundler-1.14.6
Installing ri documentation for bundler-1.14.6
Done installing documentation for bundler after 5 seconds
1 gem installed

> which bundler
Output: /Users/savita/.rvm/gems/ruby-2.4.0/bin/bundler
```

This bundler gem reads a configuration file namely "Gemfile". A very good ducumentation exists [here](http://bundler.io/gemfile.html).

Sample Gemfile looks like this.

*(Line numbers are inserted for readability purposes only. Please remove them if you want to try)*
```
1: source 'https://rubygems.org'
2: gem 'nokogiri', '~> 1.6.1'
3: gem 'logging'
```

Line 1: explains the source where we think these gems exist. In this example, I am sure that gem nokogiri exists in https://rubygems.org . Override this URL is needed.

Line 2: Here I mentioned the gem that I need and its specific version. Here I choose version greater than 1.6.1)

Line 3: Here I am trying to say that I need this gem. I do not have a specific need to choose a specific
version. Bundler will choose most latest available gem from URL mentioned in Line:1

Now, to invoke bundler to install Gems as per Gemfile, isuue this command

```
> gem install bundler
Output: Fetching: bundler-1.14.6.gem (100%)
Successfully installed bundler-1.14.6
Parsing documentation for bundler-1.14.6
Installing ri documentation for bundler-1.14.6
1 gem installed
```

Now, to see the beauty of bundler, locate for a auto generated file namely "Gemfile.lock". This file is a snapshot of all the gems and their dependency gems which bundler planned before installation are documented. For a bigger project it is worth source controlling this file for reference purposes.

```
osxuk43330:ruby ssundarababu$ cat Gemfile.lock
GEM
  remote: https://rubygems.org/
  specs:
    little-plugger (1.1.4)
    logging (2.2.2)
      little-plugger (~> 1.1)
      multi_json (~> 1.10)
    mini_portile2 (2.1.0)
    multi_json (1.12.1)
    nokogiri (1.6.8.1)
      mini_portile2 (~> 2.1.0)

PLATFORMS
  ruby

DEPENDENCIES
  logging
  nokogiri (~> 1.6.1)

BUNDLED WITH
   1.14.6
```

Now that gems are installed using bundler.

To view where ruby gems are installed, use this command
```
> bundle show logging
Output: ~/.rvm/gems/ruby-2.4.0/gems/logging-2.2.2
```
Please note that if gems are installed directly using  "gem install <gem name>",  then gems are stored in ~/.rvm/gems/ruby-2.4.0/bin, which is different than the location used by bundler.
